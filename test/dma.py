from udmabuf import Udmabuf
from devmem import Devmem
import numpy as np
import os
import time

class DMA:
    def __init__(self, chan_dir, buf):
        self.chan_dir = chan_dir
        if not os.path.isdir(self.chan_dir):
            raise Exception("ERROR:: Channel does not exist")
        # print(type(buf))
        # if (type(buf) is not Devmem) or (type(buf) is not Udmabuf):
        #     raise Exception("ERROR:: Unsupported buffer type")
        self.buf = buf
        self.sgent_dir = None
        self.add_sgent("one")

    def setup(self, length, dir, phys_addr=None):
        if self.sgent_dir is None:
            Exception("ERROR: sgent_dir is None")

        if length > self.buf.buf_size:
            Exception("ERROR: length is larger than buffer size")

        dev = os.open(f"{self.chan_dir}/direction", os.O_RDWR)
        os.write(dev, str.encode(f"{dir}\n"))
        os.close(dev)

        dev = os.open(f"{self.sgent_dir}/length", os.O_RDWR)
        os.write(dev, str.encode(f"{length}\n"))
        os.close(dev)

        dev = os.open(f"{self.sgent_dir}/src_addr", os.O_RDWR)
        os.write(dev, str.encode(f"{hex(self.buf.phys_addr)}\n"))
        os.close(dev)

    def add_sgent(self, sgent_dir):
        self.sgent_dir = f"{self.chan_dir}/{sgent_dir}"
        if not os.path.isdir(self.sgent_dir):
            os.mkdir(self.sgent_dir)

    def del_sgent(self):
        if os.path.isdir(self.sgent_dir):
            os.rmdir(self.sgent_dir)

    def start(self):
        if self.sgent_dir is None:
            Exception("ERROR: sgent_dir is None")
        
        dev = os.open(f"{self.chan_dir}/start", os.O_WRONLY)
        os.write(dev, str.encode(f"1\n"))
        os.close(dev)

    def reset(self):
        dev = os.open(f"{self.chan_dir}/reset", os.O_WRONLY)
        os.write(dev, str.encode(f"1\n"))
        os.close(dev)

    def print(self):
        if self.sgent_dir is None:
            Exception("ERROR: sgent_dir is None")
        
        print(self.sgent_dir)
        dev = os.open(f"{self.chan_dir}/direction", os.O_RDWR)
        print(f"direction={os.read(dev, 4096).decode('utf-8')}", end='')
        os.close(dev)

        dev = os.open(f"{self.sgent_dir}/length", os.O_RDWR)
        print(f"length={int(os.read(dev, 4096).decode('utf-8'))}")
        os.close(dev)

        dev = os.open(f"{self.sgent_dir}/src_addr", os.O_RDWR)
        print(f"src_addr={os.read(dev, 4096).decode('utf-8')}")
        os.close(dev)

    def init_buf(self, words, fill=False):
        test_dtype = np.uint32
        max_words = int(self.buf.buf_size/(np.dtype(test_dtype).itemsize))
        # print(f"max_words={max_words}")
        if words > max_words:
            Exception("ERROR: init_buf words exceeds buf_size")
        self.mmarray = self.buf.memmap(dtype=test_dtype, shape=(max_words))

        for index in range(words):
            if fill:
                self.mmarray[index] = index
    
    def print_buf(self, words):
        if self.buf is not None:
            print(f"{self.buf.name}  0x{self.buf.phys_addr:016x}")
            for index in range(words):
                print(f"mem[{index}]={self.mmarray[index]}")
        else:
            Exception("ERROR: buf not defined")





