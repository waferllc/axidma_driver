import os
import mmap
import numpy as np

class Devmem:
    def __init__(self, filename, base_offset, length=4096, dtype=np.uint32):

        if base_offset < 0 or length < 0: 
            raise Exception("Invalid paramters")

        self.filename = filename
        self.name = self.filename
        self.page_base = base_offset & ~(mmap.PAGESIZE - 1)
        self.phys_addr = self.page_base
        self.page_offset = base_offset - self.page_base
        self.length = length
        self.buf_size = length
        self.word_size = np.dtype(dtype).itemsize

        self.page_offset_length = self.page_offset + self.length
        self.length_words = int(self.length / self.word_size)
        self.page_offset_words = int(self.page_offset / self.word_size)
        self.page_offset_length_words = int(self.page_offset_length / self.word_size)

        # print(f"page_base=0x{self.page_base:08x}")
        # print(f"page_offset=0x{self.page_offset:08x}")
        # print(f"dtype word_size = {self.word_size}")
        # print(f"length={self.length}")
        # print(f"page_offset_length={self.page_offset_length}")
        # print(f"length words={self.length_words}")
        # print(f"page_offset words={self.page_offset_words}")

        self.fd = os.open(self.filename, os.O_RDWR | os.O_SYNC)

    def __del__(self):
        if self.fd:
            os.close(self.fd)

    def memmap(self, dtype, shape):
        self.mem = mmap.mmap(self.fd, self.page_offset_length, mmap.MAP_SHARED, mmap.PROT_READ | mmap.PROT_WRITE, offset=self.page_base)
        if shape > self.page_offset_length_words:
            Exception("ERROR: Array shape exceeds buffer length")
        self.mmarray = np.ndarray((shape), dtype, self.mem)

        return self.mmarray

    # def read(self, index):
    #     if index < 0:
    #         raise Exception("Invalid paramters")

    #     offset_index = self.page_offset_words + index
    #     if offset_index > self.page_offset_length_words:
    #         raise Exception("Offset exceeds mmap length")

    #     return self.mmarray[offset_index]

    # def write(self, index, value):
    #     if index < 0:
    #         raise Exception("Invalid paramters")

    #     offset_index = self.page_offset_words + index
    #     if offset_index > self.page_offset_length_words:
    #         raise Exception("Offset exceeds mmap length")

    #     self.mmarray[offset_index] = value

if __name__ == "__main__":
    dev = Devmem("/dev/mem", 0x500000000, 4096)
    mmarray = dev.memmap(np.uint32, 128)
    print(f"mem[0]=0x{mmarray[0]:08x}")
    write_val = 0x5678
    mmarray[0] = write_val
    read_val = mmarray[0];
    if read_val == write_val:
        print(f"PASS  mem[0]=0x{read_val:08x}")
    else:
        print(f"FAIL  mem[0]=0x{read_val:08x}")
    # dev.write(0, write_val)
    # read_val = dev.read(0)
    # if read_val == write_val:
    #     print(f"PASS  mem[0]=0x{read_val:08x}")
    # else:
    #     print(f"FAIL  mem[0]=0x{read_val:08x}")