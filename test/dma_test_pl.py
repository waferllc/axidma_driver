from udmabuf import Udmabuf
from devmem import Devmem
from dma import DMA
import numpy as np
import os
import time

# Assumes u-dma-buf.ko driver loaded
# TODO: Fix hard codings...

# Using PL DDR so no need for udmabuf
# udmabuf0 = Udmabuf("udmabuf0")
# udmabuf1 = Udmabuf("udmabuf1")
devmem0 = Devmem("/dev/mem", 0x500000000, 1024*1024*128)
devmem1 = Devmem("/dev/mem", 0x500000000, 1024*1024*128)

n_words = 32
n_bytes = n_words * (np.dtype(np.int32).itemsize)
print(f"words={n_words}  bytes={n_bytes}")

# tx_dma = DMA("/configfs/dmacore2/channel0", devmem0)
# # tx_dma.add_sgent("one")
# tx_dma.setup(n_bytes, "DMA_MEM_TO_DEV")
# tx_dma.print()
# tx_dma.init_buf(32, True)
# tx_dma.start()
# tx_dma.print_buf(32)
# tx_dma.reset()
# # # tx_dma.del_sgent()

rx_dma = DMA("/configfs/dmacore2/channel1", devmem1)
# rx_dma.add_sgent("one")
rx_dma.setup(n_bytes, "DMA_DEV_TO_MEM")
rx_dma.print()
rx_dma.init_buf(32)
rx_dma.start()
rx_dma.print_buf(32)
rx_dma.reset()
# # rx_dma.del_sgent()

