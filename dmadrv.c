
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/configfs.h>
#include <linux/device.h>
//#include <linux/cdev.h>
#include <linux/platform_device.h>
#include <linux/err.h>

#include <linux/dmaengine.h>
#include <linux/dma-mapping.h>
#include <linux/slab.h>
//#include <linux/workqueue.h>
#include <linux/of.h>
#include <linux/of_dma.h>

/*
 * TODOs:
 * 1. print/log macros
 * 2. Figure out use of semaphore - specifically on device counts, can they be avoided?
 * 3. Break up into files?
 * 4. Configurable sglist generation - either add an "api" through attributes or preferably configfs implementation
 * 5. Consider using u-dma-buf instead of allocating memory and supporting mmap in this driver
*/
// #include "dmadrv.h"

#define SIMPLE

#define DRIVER_NAME "dmadrv"
#define SG_MAX_LEN 67108864
#define MAX_DEVICES 8
#define MAX_NAME_LEN 32
#define N_CHANS 2


/* 
 * dmadrv platform device data
*/
struct dmadrv_core {
	struct configfs_subsystem subsys;
	struct list_head channels;
	const char *name;

	struct platform_device *pdev;
};

/* 
 * dmadrv channel data
*/
struct dmadrv_channel {
	struct dmadrv_core *core;
	struct config_group group;
	struct list_head descriptors;
	struct list_head node;
	const char *name;

	struct device *dma_device;
	struct dma_chan *dma_channel;			
	dma_addr_t phys_addr;
	size_t size;
	// size_t alloc_size;
	dma_cookie_t cookie;
	struct completion cmp;
	enum dma_data_direction direction;
	struct scatterlist *sg_list;
	unsigned int sg_len;
};

struct dmadrv_descriptor {
	struct dmadrv_channel *channel;
	// struct config_group group;
	struct config_item item;
	struct list_head node;

	dma_addr_t src_addr;
	size_t length;
};

const char * dma_status_names[] = {
	[DMA_COMPLETE] = "DMA_COMPLETE",
	[DMA_IN_PROGRESS] = "DMA_IN_PROGRESS",
	[DMA_PAUSED] = "DMA_PAUSED",
	[DMA_ERROR]= "DMA_ERROR",
};

const char * dma_direction_names[] = {
	[DMA_BIDIRECTIONAL] = "DMA_UNSUPPORTED",
	[DMA_MEM_TO_DEV] = "DMA_MEM_TO_DEV",
	[DMA_DEV_TO_MEM] = "DMA_DEV_TO_MEM",
	[DMA_NONE] = "DMA_NONE",
};

static u32 dmadrv_core_count;

/* ConfigFS */

/* 
 * Descriptor definition
*/
static inline struct dmadrv_descriptor *to_dmadrv_descriptor(struct config_item *item)
{
	return container_of(item, struct dmadrv_descriptor, item);
}

static ssize_t dmadrv_descriptor_src_addr_show(struct config_item *item, char *page)
{
	struct dmadrv_descriptor *descriptor;
	
	descriptor = to_dmadrv_descriptor(item);
	pr_debug("%s src_addr=%pad\n", DRIVER_NAME, &descriptor->src_addr);

	return sprintf(page, "%pad\n", &descriptor->src_addr);
}

static ssize_t dmadrv_descriptor_src_addr_store(struct config_item *item, const char *page, size_t count)
{
	struct dmadrv_descriptor *descriptor = to_dmadrv_descriptor(item);
	int ret;

	ret = kstrtoull(page, 16, &descriptor->src_addr);
	if (ret)
		return ret;

	return count;
}

static ssize_t dmadrv_descriptor_length_show(struct config_item *item, char *page)
{
	struct dmadrv_descriptor *descriptor;

	descriptor = to_dmadrv_descriptor(item);
	pr_debug("%s length=%zu\n", DRIVER_NAME, descriptor->length);

	return sprintf(page, "%zu\n", to_dmadrv_descriptor(item)->length);
}

static ssize_t dmadrv_descriptor_length_store(struct config_item *item, const char *page, size_t count)
{
	struct dmadrv_descriptor *descriptor = to_dmadrv_descriptor(item);
	int ret;

	ret = kstrtoul(page, 10, &descriptor->length);
	if (ret)
		return ret;

	return count;
}

CONFIGFS_ATTR(dmadrv_descriptor_, src_addr);
CONFIGFS_ATTR(dmadrv_descriptor_, length);

static struct configfs_attribute *dmadrv_descriptor_attrs[] = {
	&dmadrv_descriptor_attr_src_addr,
	&dmadrv_descriptor_attr_length,
	NULL,
};

static const struct config_item_type dmadrv_descriptor_type = {
	.ct_attrs	= dmadrv_descriptor_attrs,
	.ct_owner	= THIS_MODULE,
};

/* 
 * DMA Channel
*/
static inline struct dmadrv_channel *to_dmadrv_channel(struct config_item *item)
{
	return container_of(to_config_group(item), struct dmadrv_channel, group);
}
 
static struct config_item * dmadrv_channel_make_item(struct config_group * group, const char * name)
{
	struct dmadrv_channel *channel = to_dmadrv_channel(&group->cg_item);
	struct dmadrv_descriptor *descriptor;

	pr_debug("%s Allocating new descriptor\n", DRIVER_NAME);
	descriptor = kzalloc(sizeof(struct dmadrv_descriptor), GFP_KERNEL);
	if (!descriptor)
		return ERR_PTR(-ENOMEM);

	descriptor->length = 0;
	descriptor->src_addr = 0;

	config_item_init_type_name(&descriptor->item, "descriptor0", &dmadrv_descriptor_type);
	
	pr_debug("%s Adding descriptor to list\n", DRIVER_NAME);
	list_add_tail(&descriptor->node, &channel->descriptors);
	channel->sg_len++;
	pr_debug("%s Channel sg_len=%d\n", DRIVER_NAME, channel->sg_len);

	pr_debug("%s Channel make item done\n", DRIVER_NAME);

	return &descriptor->item;
}

void dmadrv_channel_drop_item(struct config_group * group, struct config_item * item)
{
	struct dmadrv_channel *channel = to_dmadrv_channel(&group->cg_item);
	struct dmadrv_descriptor *descriptor;
	pr_debug("%s Drop item\n", DRIVER_NAME);

	descriptor = to_dmadrv_descriptor(item);

	pr_debug("%s Removing descriptor from list    length=%zu     %pad\n", DRIVER_NAME, descriptor->length, &descriptor->src_addr);
	list_del(&descriptor->node);

	pr_debug("%s Freeing descriptor\n", DRIVER_NAME);
	kfree(descriptor);

	channel->sg_len--;
	pr_debug("%s Channel sg_len=%d\n", DRIVER_NAME, channel->sg_len);

	return config_item_put(item);
}


static ssize_t dmadrv_channel_description_show(struct config_item *item, char *page)
{
	return sprintf(page,"%s DMA Channel\n", DRIVER_NAME);
}


static ssize_t dmadrv_channel_direction_show(struct config_item *item, char *page)
{
	struct dmadrv_channel *channel = to_dmadrv_channel(item);
	return sprintf(page, "%s\n", dma_direction_names[channel->direction]);
}

static ssize_t dmadrv_channel_direction_store(struct config_item *item, const char *page, size_t count)
{
	struct dmadrv_channel *channel = to_dmadrv_channel(item);
	enum dma_data_direction dir;
	// enum dma_data_direction {DMA_BIRECTIONAL, DMA_MEM_TO_DEV, DMA_DEV_TO_MEM, DMA_NONE };
	enum dma_data_direction i;
	size_t n;

	n = page[count-1]=='\n' ? count-1 : count;

	channel->direction = DMA_NONE;
	for (i=DMA_MEM_TO_DEV; i<DMA_NONE; i++)
		if (strncmp(dma_direction_names[i], page, n) == 0)
			channel->direction = i;

	if (dir == DMA_NONE)
		return -EINVAL;
	else 
		return count;
}

// static ssize_t dmadrv_channel_dma_cap_show(struct config_item *item, char *page)
// {
// 	struct dmadrv_channel *channel = to_dmadrv_channel(item);
// 	enum dma_descriptor_type cap;
// 	int bytes=0;

// 	for_each_dma_cap_mask(cap, channel->dma->device->cap_mask) {
// 		bytes += sprintf(page + bytes, "%s\n", dma_descriptor_names[cap]);
// 	}

// 	return bytes;
// }

static ssize_t dmadrv_channel_status_show(struct config_item *item, char *page)
{
	struct dmadrv_channel *channel = to_dmadrv_channel(item);
	struct dma_tx_state state;
	enum dma_status status; // DMA_SUCCESS, DMA_IN_PROGRESS, DMA_PAUSED, DMA_ERROR 
	int bytes=0;

	status = dmaengine_tx_status(channel->dma_channel, channel->cookie, &state);
	bytes += sprintf(page + bytes, "%s\n", dma_status_names[status]);

	return bytes;
}

static ssize_t dmadrv_channel_residue_show(struct config_item *item, char *page)
{
	struct dmadrv_channel *channel = to_dmadrv_channel(item);
	struct dma_tx_state state;
	enum dma_status status;
	int bytes=0;

	status = dmaengine_tx_status(channel->dma_channel, channel->cookie, &state);
	bytes += sprintf(page + bytes, "%u\n", state.residue);

	return bytes;
}

static ssize_t dmadrv_channel_start_store(struct config_item *item, const char *page, size_t count)
{
	struct dmadrv_channel *channel = to_dmadrv_channel(item);
	int err;

	struct dma_chan *dma_channel;
	struct scatterlist *sg_list, *sgent;
	struct dma_async_tx_descriptor *desc;
	dma_cookie_t cookie;

	int sg_len;
	struct dmadrv_descriptor *descriptor;
	int i;

	pr_debug("%s Starting DMA\n", DRIVER_NAME);
	
	if(sg_list)
		pr_debug("%s 1: sg_list exists\n", DRIVER_NAME);
	else
		pr_debug("%s 1: sg_list does not exists\n", DRIVER_NAME);

	pr_debug("%s Allocating sg_list\n", DRIVER_NAME);
	sg_len = channel->sg_len;
	sg_list = kzalloc(sizeof(struct scatterlist)*sg_len, GFP_KERNEL);
	if (!sg_list) {
		pr_err("%s Could not allocation sg_list", DRIVER_NAME);
		err = -ENOMEM;
		return err;
	}

	if(sg_list)
		pr_debug("%s 2: sg_list exists\n", DRIVER_NAME);
	else
		pr_debug("%s 2: sg_list does not exists\n", DRIVER_NAME);

	pr_debug("%s Initializing sg_list table\n", DRIVER_NAME);
	sg_init_table(sg_list, sg_len);
	
	pr_debug("%s Getting first descriptor\n", DRIVER_NAME);
	descriptor = list_first_entry(&channel->descriptors, struct dmadrv_descriptor, node);
	for_each_sg(sg_list, sgent, sg_len, i) {

		pr_debug("%s Assigning sgent %d\n", DRIVER_NAME, i);
		sg_dma_address(sgent) = descriptor->src_addr;
		sg_dma_len(sgent) = descriptor->length;

		// TODO: error check this with is_last?
		descriptor = list_next_entry(descriptor, node);
	}

	for_each_sg(sg_list, sgent, sg_len, i) {
		pr_debug("%s sgent %d    address=%pad    length=%u\n", DRIVER_NAME, i, &sg_dma_address(sgent), sg_dma_len(sgent));
	}
	channel->sg_list = sg_list;

	dma_channel = channel->dma_channel;
	pr_debug("%s DMA prep slave device\n", DRIVER_NAME);
	desc = dmaengine_prep_slave_sg(dma_channel, channel->sg_list, channel->sg_len, channel->direction, DMA_CTRL_ACK | DMA_PREP_INTERRUPT);
	if (!desc) {
		pr_err("%s DMAENGINE_PREP_SLAVE_SG ERROR\n", DRIVER_NAME);
		err = -EPERM;
		goto stop_dma;
	} 

	pr_debug("%s Submitting descriptor\n", DRIVER_NAME);
	cookie = dmaengine_submit(desc);
	if (dma_submit_error(cookie)) {
		pr_err("%s UNABLE TO SUBMIT THE TRANSACTION TO THE ENGINE\n", DRIVER_NAME);
		err = -EBUSY;
		goto stop_dma;
	}
	channel->cookie = cookie;

	pr_debug("%s Start DMA transaction\n", DRIVER_NAME);
	dma_async_issue_pending(dma_channel);

	pr_debug("%s DMA transaction started successfully\n", DRIVER_NAME);

	return count;

stop_dma:
	dmaengine_terminate_all(channel->dma_channel);
	kfree(channel->sg_list);
	return err;
}

static ssize_t dmadrv_channel_reset_store(struct config_item *item, const char *page, size_t count)
{
	struct dmadrv_channel *channel = to_dmadrv_channel(item);
	int err;
	struct scatterlist *sg_list;

	pr_debug("%s Resetting DMA start\n", DRIVER_NAME);

	dmaengine_terminate_all(channel->dma_channel);
	
	sg_list = channel->sg_list;

	if(sg_list) 
		pr_debug("%s sg_list exists\n", DRIVER_NAME);
	else
		pr_debug("%s sg_list does not exist\n", DRIVER_NAME);
	kfree(sg_list);

	pr_debug("%s Resetting DMA end\n", DRIVER_NAME);

	return count;
}

static ssize_t dmadrv_channel_list_show(struct config_item *item, char *page)
{
	struct dmadrv_channel *channel = to_dmadrv_channel(item);
	struct dmadrv_descriptor *descriptor;
	struct scatterlist *sg_list, *sgent;
	int sg_len;
	
	int i;
	int err;
	int bytes=0;

	sg_len = channel->sg_len;
	pr_debug("%s Allocating sg_list with sg_len=%d\n", DRIVER_NAME, sg_len);
	sg_list = kzalloc(sizeof(struct scatterlist)*sg_len, GFP_KERNEL);
	if (!sg_list) {
		pr_err("%s Could not allocation sg_list", DRIVER_NAME);
		return -ENOMEM;
	}

	pr_debug("%s Initializing sg_list table\n", DRIVER_NAME);
	sg_init_table(sg_list, sg_len);

	pr_debug("%s Getting first descriptor\n", DRIVER_NAME);
	descriptor = list_first_entry(&channel->descriptors, struct dmadrv_descriptor, node);
	for_each_sg(sg_list, sgent, sg_len, i) {

		pr_debug("%s Assigning sgent %d\n", DRIVER_NAME, i);
		sg_dma_address(sgent) = descriptor->src_addr;
		sg_dma_len(sgent) = descriptor->length;

		// TODO: error check this with is_last?
		descriptor = list_next_entry(descriptor, node);
	}

	for_each_sg(sg_list, sgent, sg_len, i) {
		bytes += sprintf(page + bytes, "%4d; src_addr=%pad; length=%u;\n", i,
				 &sg_dma_address(sgent), sg_dma_len(sgent));
	}

	kfree(sg_list);
	return bytes;

	// i = 0;
	// list_for_each_entry(descriptor, &channel->descriptors, node) {
	// 	pr_debug("%s Descriptor %d    length=%zu    src_addr=%pad\n", DRIVER_NAME, i, descriptor->length, &descriptor->src_addr);
	// 	i++;
	// }

	// return sprintf(page, "descriptor list \n");
}

CONFIGFS_ATTR_RO(dmadrv_channel_, description);
CONFIGFS_ATTR(dmadrv_channel_, direction);
// CONFIGFS_ATTR_RO(dmadrv_channel_, dma_cap);
CONFIGFS_ATTR_RO(dmadrv_channel_, status);
CONFIGFS_ATTR_RO(dmadrv_channel_, residue);
CONFIGFS_ATTR_WO(dmadrv_channel_, start);
CONFIGFS_ATTR_WO(dmadrv_channel_, reset);
CONFIGFS_ATTR_RO(dmadrv_channel_, list);

static struct configfs_attribute *dmadrv_channel_attrs[] = {
	&dmadrv_channel_attr_description,
	&dmadrv_channel_attr_list,
	&dmadrv_channel_attr_direction,
	// &dmadrv_channel_attr_dma_cap,
	&dmadrv_channel_attr_status,
	&dmadrv_channel_attr_residue,
	&dmadrv_channel_attr_start,
	&dmadrv_channel_attr_reset,
	NULL,
};

static struct configfs_group_operations dmadrv_channel_group_ops = {
	.make_item	= dmadrv_channel_make_item,
	.drop_item = dmadrv_channel_drop_item,
};

static const struct config_item_type dmadrv_channel_type = {
	.ct_group_ops	= &dmadrv_channel_group_ops,
	.ct_attrs	= dmadrv_channel_attrs,
	.ct_owner	= THIS_MODULE,
};

/*
 * DMA Subsystem
*/
static ssize_t dmadrv_core_description_show(struct config_item *item, char *page)
{
	return sprintf(page,"%s DMA core subsystem\n", DRIVER_NAME);
}

CONFIGFS_ATTR_RO(dmadrv_core_, description);

static struct configfs_attribute *dmadrv_core_attrs[] = {
	&dmadrv_core_attr_description,
	NULL,
};

static const struct config_item_type dmadrv_core_type = {
	.ct_attrs	= dmadrv_core_attrs,
	.ct_owner	= THIS_MODULE,
};

/*
 * Device Probe
*/ 
static int dmadrv_probe(struct platform_device *pdev)
{
	struct dmadrv_core *core;
	struct dmadrv_channel *channel;
	struct dma_chan *dma_channel;

	int err;
	int n_chans;
	int c;

	const char *core_name;
	const char *chan_name;

	const char *dt_chan_name;

	pr_debug("%s Probe start", DRIVER_NAME);

	pr_debug("%s Allocating core system\n", DRIVER_NAME);
	core = kzalloc(sizeof(struct dmadrv_core), GFP_KERNEL);
	if (core == NULL) {
		pr_err("%s UNABLE TO ALLOCATE THE PLATFORM DEVICE STRUCTURE\n", DRIVER_NAME);
		err = -ENOMEM;
		return err;
	}

	pr_debug("%s Allocating core name\n", DRIVER_NAME);
	core_name = kmalloc(MAX_NAME_LEN, GFP_KERNEL);
	if (core_name == NULL) {
		pr_err("%s Error while allocating channel name; using default\n", DRIVER_NAME);
		core_name = "noname";
	}
	else {
		snprintf(core_name, MAX_NAME_LEN, "dmacore%d", dmadrv_core_count);
	}
	core->name = core_name;
	core->pdev = pdev;
	pr_debug("%s Created core name %s\n", DRIVER_NAME, core_name);

	pr_debug("%s Initializing core channels list\n", DRIVER_NAME);
	INIT_LIST_HEAD(&core->channels);

	pr_debug("%s Initializing core subsystem\n", DRIVER_NAME);
	config_group_init_type_name(&core->subsys.su_group, core_name, &dmadrv_core_type);
	pr_debug("%s Initializing core subsystem mutex\n", DRIVER_NAME);
	mutex_init(&core->subsys.su_mutex);

	pr_debug("%s Registering configfs subsystem start\n", DRIVER_NAME);
	err = configfs_register_subsystem(&core->subsys);
	if (err) {
		pr_err("%s ERROR WHILE REGISTERING SUBSYSTEM %s\n", DRIVER_NAME, core->subsys.su_group.cg_item.ci_namebuf);
		goto free_core;
	}

	pr_debug("%s Reading number of channels from DT\n", DRIVER_NAME);
	n_chans = of_property_count_strings(pdev->dev.of_node, "dma-names");
	pr_debug("%s Found %d channels\n", DRIVER_NAME, n_chans);
	for(c=0; c<n_chans; c++) {
		pr_debug("%s Allocating channel\n", DRIVER_NAME);
		channel = kzalloc(sizeof(struct dmadrv_channel), GFP_KERNEL);
		if (channel == NULL) {
			pr_err("%s NO MEMORY FOR CHANNEL %s\n", DRIVER_NAME, dma_chan_name(dma_channel));
			err = -ENOMEM;
			goto free_core;
		}

		pr_debug("%s Allocating channel name\n", DRIVER_NAME);
		chan_name = kmalloc(MAX_NAME_LEN, GFP_KERNEL);
		if (chan_name == NULL) {
			pr_err("%s ERROR WHILE ALLOCATING CHANNEL NAME; USING DEFAULT\n", DRIVER_NAME);
			chan_name = "noname";
		}
		else {
			snprintf(chan_name, MAX_NAME_LEN, "channel%d", c);
		}
		channel->name = chan_name;
		pr_debug("%s Created channel name %s\n", DRIVER_NAME, chan_name);

		pr_debug("%s Initializing channel %d descriptors list\n", DRIVER_NAME, c);
		INIT_LIST_HEAD(&channel->descriptors);
		channel->sg_len = 0;

		pr_debug("%s Initializing channel %d group\n", DRIVER_NAME, c);
		config_group_init_type_name(&channel->group, chan_name, &dmadrv_channel_type);

		pr_debug("%s Registering channel %d group\n", DRIVER_NAME, c);
		err = configfs_register_group(&core->subsys.su_group, &channel->group);
		if (err) {
			pr_err("%s ERROR WHILE REGISTERING GROUP\n", DRIVER_NAME);
			goto free_chan;
		}

		// TODO: Add this as a RO attribute to the channel
		of_property_read_string_index(core->pdev->dev.of_node, "dma-names", c, &dt_chan_name);
		pr_info("%s found channel name %s\n", DRIVER_NAME, dt_chan_name);
		pr_debug("%s Requesting DMA channel %s\n", DRIVER_NAME, dt_chan_name);
		dma_channel = dma_request_chan(&pdev->dev, dt_chan_name);
		if (IS_ERR(dma_channel)) {
			err = PTR_ERR(dma_channel);
			if (err != -EPROBE_DEFER)
				pr_err("%s NO CHANNEL\n", DRIVER_NAME);
			goto free_group;
		}
		channel->dma_channel = dma_channel;
		pr_debug("%s Got DMA channel %s %s\n", DRIVER_NAME, channel->name, dma_chan_name(dma_channel));

		pr_debug("%s Adding channel %s to list of channels\n", DRIVER_NAME, channel->name);
		list_add_tail(&channel->node, &core->channels);
	}

	pr_debug("%s Saving core to platform device data\n", DRIVER_NAME);
	dev_set_drvdata(&pdev->dev, core);
	
	// TODO: Semaphore
	dmadrv_core_count++;

	pr_debug("%s Probe end", DRIVER_NAME);

	return 0;

free_group:
	configfs_unregister_group(&channel->group);

free_chan:
	// dma_release_channel(dma_channel);
	// TODO: Free created channels? This is better in a function...
	kfree(channel->name);
	kfree(channel);

free_core:
	kfree(core->name);
	kfree(core);
	return err;
}

static int dmadrv_remove(struct platform_device *pdev)
{
	struct dmadrv_core *core;
	struct dmadrv_channel *channel, *tmp;
	struct dma_chan *dma_channel;

	int c=0;

	pr_debug("%s Remove start", DRIVER_NAME);

	pr_debug("%s Getting core from platform device data", DRIVER_NAME);
	core = dev_get_drvdata(&pdev->dev);

	list_for_each_entry_safe(channel, tmp, &core->channels, node) {
		pr_debug("%s Removing channel %s from list", DRIVER_NAME, channel->name);
		list_del(&channel->node);

		dma_channel = channel->dma_channel;
		pr_debug("%s Releasing DMA channel %s %s\n", DRIVER_NAME, channel->name, dma_chan_name(dma_channel));
		dmaengine_terminate_all(dma_channel);
		dma_release_channel(dma_channel);

		pr_debug("%s Unregistering channel %d group\n", DRIVER_NAME, c);
		configfs_unregister_group(&channel->group);

		pr_debug("%s Freeing channel %s\n", DRIVER_NAME, channel->name);
		kfree(channel->name);
		kfree(channel);

		c++;
	}

	pr_debug("%s Unregistering core subsystem\n", DRIVER_NAME);
	configfs_unregister_subsystem(&core->subsys);

	pr_debug("%s Freeing platform device data", DRIVER_NAME);
	kfree(core->name);
	kfree(core);

	pr_debug("%s Remove end", DRIVER_NAME);

	return 0;
}

/*
 * Platform driver
*/
static const struct of_device_id dmadrv_of_ids[] = {
	// { .compatible = "xlnx,dmadrv",},
	{ .compatible = "xlnx,axi-dma-test-1.00.a",},
	{}
};

static struct platform_driver dmadrv_driver = {
	.driver = {
		.name = "dmadrv_driver",
		.owner = THIS_MODULE,
		.of_match_table = dmadrv_of_ids,
	},
	.probe = dmadrv_probe,
	.remove = dmadrv_remove,
};

static int __init dmadrv_init(void){
	pr_debug("%s Init start\n", DRIVER_NAME);

	pr_debug("%s Registering platform driver\n", DRIVER_NAME);
    return platform_driver_register(&dmadrv_driver);
}
// late_initcall(dmadrv_init);

static void __exit dmadrv_exit(void)
{
	pr_debug("%s Exit start\n", DRIVER_NAME);
	
	pr_debug("%s Unregistering platform driver\n", DRIVER_NAME);
	platform_driver_unregister(&dmadrv_driver);
	
	pr_debug("%s Exit done\n", DRIVER_NAME);
}

module_init(dmadrv_init);
module_exit(dmadrv_exit);
 
MODULE_LICENSE("GPL");
