# Xilinx AXIDMA core in FPGA PL  

* Scatter gather support  
* 26-bit descriptor length (64MB per SG entry)  
* Very high performance (even though we don’t need it)  

## Mostly generic user driver kernel module – only uses DMA Engine API  
* Right now it is a bit crude with room for lots of feature enhancements  
* FPGA devices are all platform drivers that get probed when loaded, either on boot or with overlay  

## ** OLD ** From Ultra96, or for PS DDR in ZCU208
Uses u-dma-buf to mmap buffers in CMA region (size is enabled in kernel – currently using 1GB)  
* https://github.com/ikwzm/udmabuf  
* Avoids mapping in the kernel module  
* Scatter gather descriptors are kmalloc’d  

## ** NEW ** ZCU208 RFSoC implementation devmem's memory in PL DDR instead of using u-dma-buf  

## ConfigFS (instead of char dev) to configure and control DMA   
* DMA cores and channels are autloaded under /configfs  
* DMA Core and Channel information is read from the device tree 
* Descriptors are added by the user under each channel to create scatter gather chain  
* Descriptors include src_address and length  
* Start, reset, directions, status attributes under each channel  

## User application c or python to setup and control DMA and u-dma-buf    

## Tested and working:   
* TX – RX loopback in FPGA  
* 128MB transfers with 1 and 2 SG entries  
* Initialized Tx and Rx buffers, ran DMA, checked RX buffer for correct data  
* Tested with ZCU208 using 4 DAC and 4 ADC with 128MB transfers - no observed loss of data (other than initial stale data in interconnect at the beginning - this gets chopped off in software)  

ConfigFS Example:
/configfs/​

 |---- dmacore0/​

          |---- channel0/​

                   |---- descriptor0/​

                            |---- src_address​

                            |---- length​

                   |---- descriptor1/​

                            |---- src_address​

                            |---- length​

                   |---- status​

                   |---- start​

                   |---- reset​

                   |---- list​

                   |---- direction​

          |---- channel2/​

                   |---- descriptor0/​

                            |---- src_address​

                            |---- length​

                   |---- descriptor1/​

                            |---- src_address​

                            |---- length​

                   |---- status​

                   |---- start​

                   |---- reset​

                   |---- list​

                   |---- direction​

  |---- dmacore1/ ….​